Kata Checkout

#[Kata09: Back to the Checkout](http://codekata.com/kata/kata09-back-to-the-checkout/)
Back to the supermarket. This week, we’ll implement the code for a checkout system that handles pricing schemes such as “apples cost 50 cents, three apples cost $1.30.”

Way back in KataOne we thought about how to model the various options for supermarket pricing. We looked at things such as “three for a dollar,” “$1.99 per pound,” and “buy two, get one free.”

This week, let’s implement the code for a supermarket checkout that calculates the total price of a number of items. In a normal supermarket, things are identified using Stock Keeping Units, or SKUs. In our store, we’ll use individual letters of the alphabet (A, B, C, and so on). Our goods are priced individually. In addition, some items are multipriced: buy n of them, and they’ll cost you y cents. For example, item ‘A’ might cost 50 cents individually, but this week we have a special offer: buy three ‘A’s and they’ll cost you $1.30. In fact this week’s prices are:

  Item   Unit      Special
         Price     Price
  --------------------------
    A     50       3 for 130
    B     30       2 for 45
    C     20
    D     15

## Solution Description
- Created three classes of CheckOut , PricePolicy and Discount.
- Inside the ```Rules``` , every product is bound with its pricying policy and discount.
- Checkout class contains all items and calculates their total and each product give price as per their pricing policy and discount on
  specifed quanity.
- When checkout object is initialized , it get all predefined ```Rules``` and items hash (cart) is initally empty
- Once specs trigger scan for item , it keeps adding items in hash.
- Total function calculates total of prices cart/items by invoking each iterm price policy and discount if its available for that item

## Setup

1. Make sure you have Ruby 2.2 installed in your machine. If you need help installing Ruby, take a look at the [official installation guide](https://www.ruby-lang.org/en/documentation/installation/).

2. Install the [bundler gem](http://bundler.io/) by running:

    ```gem install bundler```

3. Clone this repo:

    ```git clone git@bitbucket.org:umairejaz/checkout_kata.git```

4. Change to the app directory:

    ```cd checkout_kata```

5. Install dependencies:

    ```bundle install```

And you're ready to go!

### Running the tests:
```bundle exec rspec spec/checkout_spec.rb```